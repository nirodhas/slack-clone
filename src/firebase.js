import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCzyPx4PeNhdxtFnhRFTaNw6F0Jkoy8iM4",
    authDomain: "clone-fd17a.firebaseapp.com",
    databaseURL: "https://clone-fd17a.firebaseio.com",
    projectId: "clone-fd17a",
    storageBucket: "clone-fd17a.appspot.com",
    messagingSenderId: "610736634293",
    appId: "1:610736634293:web:1c1a8c60ec76eeb99041fc",
    measurementId: "G-C3M1C5ZVPJ"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebaseApp.firestore();
  const auth = firebase.auth();
  const provider = new firebase.auth.GoogleAuthProvider();

  export {auth, provider, db};